---
Title: About
Authors: 
    - Louis Vigneras
toc: true
---
# About

This study was conducted as part of a class named *Decoding AI Biases* taught by Jean-Philippe Cointet (helped by Camille Chanial, the TA) at Sciences Po Paris in the *digital, new technology, and public policy* stream.

Anthony Ammendolea, Hamza Belgroun, Tom Foures, Nicolas Julian, and Louis Vigneras are the authors of this study. 

The instructions given were the following as *per* the syllabus:

> identify/generate a dataset online, design an experimental plan to analyze its inherent biases, and finally visualize and reflect upon the systematic discriminations embedded in the dataset. The final delivery will take the form of a website.

## Diclaimer and Copyright

This goes without saying, but in order to make sure there is no confusion, all the content present does not represent in any form the opinions of Sciences Po, the teachers, nor MISTRAL AI; they only represent the opinions of the authors at the time of writing.

??? warning "Todo"
    Add copyright licence.

To render bibliographic ressources, we used the `mkdocs-bibtex` from [ Shyam Dwaraknath](https://github.com/shyamd) and the CLS style files from the [CLS projet](https://citationstyles.org/).